import React from 'react'
import { Image } from 'react-native'
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation'

// Screen
import Home from '../../screens/Home/index'
import Account from '../../screens/Account/index'
import Splash from '../../screens/Splash/index'


import { Icon } from 'react-native-elements'



// Screen
const TABS = createBottomTabNavigator({
    Home: {
        screen: Home,
        path: 'home',
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({ focused }) =>  (
                focused
                ? <Icon type="MaterialCommunityIcons" name='home' size={30} color='#DB213F' />
                : <Icon type="MaterialCommunityIcons" name='home' size={30} color='#000' />
            )
        }
    },
    Account: {
        screen: Account,
        path: 'account',
        navigationOptions: {
            tabBarLabel: 'Akun',
            tabBarIcon: ({ focused }) =>  (
                focused
                ? <Icon type="MaterialCommunityIcons" name='account-circle' size={30} color='#DB213F' />
                : <Icon type="MaterialCommunityIcons" name='account-circle' size={30} color='#000' />
            )
        }
    }
}, 
{
    tabBarOptions: {
        showLabel: false,
        activeTintColor: '#0066b3',
        inactiveTintColor: '#000',
        labelStyle: {
            letterSpacing: 0.7,
            fontSize: 12,
            fontWeight: '400',
            paddingTop: 5
        },
        style: {
            backgroundColor: '#ffffff', 
            paddingTop: 5
        },
        tabStyle: {
            margin: 0,
            padding: 0
        }
    },
    
})


const STACK = createStackNavigator({
    Tab: {
        screen: TABS,
        navigationOptions:{
            header:null
        }
    },
    Splash:  {
        screen: Splash,
        navigationOptions:{
            header:null
        }
    }
    
}, {
    initialRouteName: 'Splash'
})

export default createAppContainer(STACK)    