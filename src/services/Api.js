import { baseURL, ApiKey } from '../base/constant/index'

export async function fetchTopRatedMovie(data) {
    const headers = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    }
    const response = await fetch(baseURL + "movie/top_rated?api_key=" + ApiKey + "&language=en-US&page=1", headers);
    return handleResponse(response);
}

export async function fetchUpcomingMovie(data) {
    const headers = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    }
    const response = await fetch(baseURL + "movie/now_playing?api_key=" + ApiKey + "&language=en-US&page=1", headers);
    return handleResponse(response);
}


export async function fetchNowPlayingMovie(data) {
    const headers = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    }
    const response = await fetch(baseURL + "movie/upcoming?api_key=" + ApiKey + "&language=en-US&page=1", headers);
    return handleResponse(response);
}


export async function fetchPopularMovie(data) {
    const headers = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    }
    const response = await fetch(baseURL + "movie/popular?api_key=" + ApiKey + "&language=en-US&page=1", headers);
    return handleResponse(response);
}

export async function fetchPopularTVShows(data) {
    const headers = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    }
    const response = await fetch(baseURL + "tv/popular?api_key=" + ApiKey + "&language=en-US&page=1", headers);
    return handleResponse(response);
}



function handleResponse(response) {
    console.log(response)
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        console.log(response)
        console.log(data)
        if (!response.ok) {
            // if (response.status === 401) {
            //     // auto logout if 401 response returned from api
            //     logout();
            //     location.reload(true);
            // }

            // const error = (data && data.message) || response.statusText;
            // return Promise.reject(error);
        }
        return data;
    });
}