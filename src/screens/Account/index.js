import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import Header  from '../../components/Header'
import { Grid, Row, Col } from 'react-native-easy-grid'

class Account extends React.Component {

    render() {
        return(
            <ScrollView style={{ backgroundColor: "#f4f4f4" }}>
                <Header/>
                <Grid style={{ paddingTop: 50, justifyContent: "center", alignItems: "center" }}>
                    <Row>
                        <Text style={{ fontWeight: "400", fontSize: 18 }}>
                            Name : Eka Sundana Hasibuan
                        </Text>
                    </Row>
                    <Row>
                        <Text style={{ fontWeight: "400", fontSize: 18 }}>
                            Email : sundanaeka@gmail.com
                        </Text>
                    </Row>
                    <Row>
                        <Text style={{ fontWeight: "400", fontSize: 18 }}>
                            Phone : +62 82116135446
                        </Text>
                    </Row>
                </Grid>
            </ScrollView>
        )
    }
}


export default Account