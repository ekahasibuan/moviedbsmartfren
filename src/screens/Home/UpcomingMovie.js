import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Grid, Row, Col } from 'react-native-easy-grid'
import { assetURL } from '../../base/constant'

class UpcomingMovie extends React.PureComponent {

    
    render() {
        console.log("disini")
        console.log(this.props)
        return(
            <Grid style={{ marginBottom : 20 }}>
                <Row>
                    <Text style={{ fontWeight: "bold", fontSize: 18, color: "#333333" }}>Cooming Soon</Text>
                </Row>
                <Row style={{ marginTop: 10 }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {
                            
                            this.props.data && this.props.data.map((data, index) => {
                                let imageSource = assetURL + data.poster_path
                                return (
                                    
                                    <Grid key={index} style={{ backgroundColor: "#FFFFFF", width: 200, height: 300, marginRight: 10 }}>
                                        <Image resizeMode="cover" style={{ height: 250, width: 200}} 
                                            source={{uri: imageSource}}
                                        />
                                        <View style={{ height: 50, width: 200, position: "absolute", bottom: 0,backgroundColor: "#DB213F", justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ color: "#FFFFFF", fontSize: 16, fontWeight: "500" }}>BUY TICKET</Text>
                                        </View>
                                    </Grid>
                                )
                            })
                        }
                    </ScrollView>
                </Row>
            </Grid>
        )
    }
}


export default UpcomingMovie