import React from 'react'
import { ScrollView } from 'react-native'
import Header  from '../../components/Header'
import { connect } from 'react-redux'

import {
    getTopRatedMovie,
    getUpcomingMovie,
    getNowPlayingMovie,
    getPopularMovie,
    getPopularTV
} from '../../redux/actions/globalAction'

import NowPlaying from './NowPlaying'
import PopularTv from './PopularTv'
import PopularMovie from './PopularMovie'
import TopRatedMovie from './TopRatedMovie'
import UpcomingMovie from './UpcomingMovie'

class Home extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(getNowPlayingMovie())
        this.props.dispatch(getUpcomingMovie())
        this.props.dispatch(getTopRatedMovie())
        this.props.dispatch(getPopularMovie())
        this.props.dispatch(getPopularTV())
    }

    

    render() {
        return(
            <ScrollView style={{ backgroundColor: "#F5F5F5" }}>
                <Header/>
                <ScrollView style={{ padding: 20 }}>
                    <NowPlaying data={this.props.NowPlayingMovie}/>
                    <PopularMovie data={this.props.PopularMovie}/>
                    <TopRatedMovie data={this.props.TopRatedMovie}/>
                    <UpcomingMovie data={this.props.UpcomingMovie}/>
                    
                    <PopularTv data={this.props.PopularTVShows}/>
                </ScrollView>
            </ScrollView>
        )
    }
}

function stateToProps(state) {
    return {
        TopRatedMovie: state.global.TopRatedMovie,
        UpcomingMovie: state.global.UpcomingMovie,
        NowPlayingMovie: state.global.NowPlayingMovie,
        PopularMovie: state.global.PopularMovie,
        PopularTVShows: state.global.PopularTVShows
    }
}

export default connect(stateToProps)(Home)