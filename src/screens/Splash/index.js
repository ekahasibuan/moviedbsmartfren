import React from 'react'
import { View, Text, Image } from 'react-native'
import { NavigationActions, StackActions } from 'react-navigation'

class Splash extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        setTimeout(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'Tab'
                  }),
                ],
            });
            this.props.navigation.dispatch(resetAction);
        }, 1500)
    }
    

    render() {
        return(
            <View style={{ backgroundColor: "#DB213F", flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Image style={{ width: 300, height: 300 }} resizeMode="center" source={require("../../assets/logo.png")} />
            </View>
        )
    }
}


export default Splash