import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet, Dimensions, Image } from 'react-native'
import { Grid, Row, Col } from 'react-native-easy-grid'
import Icon from 'react-native-vector-icons/FontAwesome'


const window = Dimensions.get('window')
let deviceHeight = Dimensions.get('window').height
var deviceWidth = Dimensions.get('window').width

const Button = (props) => {
    return (
        <View>
            <TouchableOpacity style={styles.sectionButton}
                onPress={() => props.onClick()} >
                <Text style={styles.buttonLabel}>
                    {props.title}
                </Text>
            </TouchableOpacity>
        </View>
    )
}




const styles = StyleSheet.create({
    sectionButton: { backgroundColor: "#ed6d00", alignItems: "center", justifyContent: "center", padding: 10, borderRadius: 6, marginLeft: 20, marginRight: 20 },
    buttonLabel: { color: "#FFFFFF", fontSize: 16 },
})

export { Button  }