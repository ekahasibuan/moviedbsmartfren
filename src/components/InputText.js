import React from 'react'
import {  View, Text, TextInput, StyleSheet, Dimensions, Image } from 'react-native'
import { Grid, Col} from 'react-native-easy-grid'


let deviceHeight = Dimensions.get('window').height
var deviceWidth = Dimensions.get('window').width

const InputText = (props) => {
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                keyboardType={(props.keyboardType) ? props.keyboardType : "default" } 
                maxLength={(props.maxLength) ? props.maxLength : 100}
                placeholder={props.placeholder}
                secureTextEntry={(props.isPassword) ? props.isPassword : false }
                placeholderTextColor="#000"
                value={ (props.value) ? props.value : null }
                editable={props.editable}
                onChangeText={(text) => {
                    props.onChangeText(text)
                }}
            />
        </View>
      )
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 20,
        marginRight: 20
    },
    label: {
        color: '#838383',
        paddingBottom: 5
    },
    input: {
        color: "#000",
        paddingLeft: 10,
        paddingRight: 10,
        height: 40, 
        backgroundColor: "#FFFFFF",
        borderColor: '#e6e6e6', 
        borderWidth: 0.5,
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomRightRadius: 6,
        borderBottomLeftRadius: 6
    },
})

export  { InputText }
