import React from 'react'
import { View, Text, Image } from 'react-native'
import { Header } from 'react-native-elements'
import { Row, Col } from 'react-native-easy-grid'


const HeaderCenter = () => {
    return (
        <Row>
            <Col style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontSize: 20, color: "#ffffff", fontWeight: "bold" }}>Smartfren</Text>
            </Col>
        </Row>
    )
}



const HeaderBar = () => (
    <Header
        centerComponent={ <HeaderCenter/>}
        containerStyle={{
            backgroundColor: '#DB213F',
            paddingTop: 0,
            height: 50
        }}
    />
);


export default HeaderBar

