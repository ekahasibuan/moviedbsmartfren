import React from 'react'
import { View, Text, Dimensions, StyleSheet } from 'react-native'
// import { Grid, Row, Col } from 'react-native-easy-grid'

const window = Dimensions.get('window')
let deviceHeight = Dimensions.get('window').height
var deviceWidth = Dimensions.get('window').width

const SubHeaderPage = (props) => (
    <View style={styles.container}>
        <Text style={styles.pageTitle}>
            Book your flight
        </Text>
    </View>
);


export default SubHeaderPage

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#00275d",
        height: deviceHeight / 4,
        borderBottomLeftRadius: 50,
        alignItems: "flex-start",
        justifyContent: "flex-start",
        paddingLeft: 20,
        paddingTop: 30
    },
    pageTitle: {
        color: "#FFFFFF",
        fontSize: 24,
        fontWeight: "400"
    }
});