import React from 'react'
import { View, Text, Image, TouchableOpacity, TouchableHighlight } from 'react-native'
import { Header, Icon } from 'react-native-elements'
import { Row, Col } from 'react-native-easy-grid'


const BackButton = (props) => {
    return (
        <TouchableOpacity onPress={()=> props.goBack() } style={{ paddingRight: 20 }}>
            <Icon name="md-arrow-back" color='#FFFFFF' type='ionicon' />
        </TouchableOpacity>
    )
}

const TitlePage = (props) => {
    return (
        <Text style={{ color: "#FFFFFF", fontSize: 18, fontWeight: "400", letterSpacing: 0.5, marginLeft: -10 }}>
            { props.title }
        </Text>
    )
} 

const HeaderRight = () => {
    return (
        <Row style={{ alignItems: "center", justifyContent: "center" }}>
            <Col>
                <Image
                    source={require('../assets/icons/notification.png')}
                    style={{ height: 22, width: 22, resizeMode: "center" }}
                />
            </Col>
            <Col>
                <Image
                    source={require('../assets/icons/chat.png')}
                    style={{ height: 22, width: 22, resizeMode: "center" }}    
                />
            </Col>
        </Row>
    )
}

const HeaderPage = (props) => (
    <Header
        placement="left"
        leftComponent={<BackButton goBack={props.callback}/>}
        centerComponent={<TitlePage title={props.title} />}
        // rightComponent={{ icon: 'home', color: '#fff' }}
        containerStyle={{
            backgroundColor: '#00275d',
            paddingTop: 0,
            borderBottomWidth: 0,
            borderBottomColor: "transparent",
            height: 50
        }}
    />
);


export default HeaderPage

