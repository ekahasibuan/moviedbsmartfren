import { combineReducers } from 'redux'

import GlobalReducer from './globalReducer'

const RootReducer = combineReducers({
  global: GlobalReducer,
})

export default RootReducer