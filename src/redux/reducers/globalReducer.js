
const INITIAL_STATE = {
    isFetching: false,
    TopRatedMovie: [],
    UpcomingMovie: [],
    NowPlayingMovie: [],
    PopularMovie: [],
    PopularTVShows: []
}

export default function GlobalReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case "GET_NOW_PLAYING_MOVIE" : {
            return {
                ...state,
                isFetching: true,
            }
        }

        case "GET_TOP_RATED_MOVIE_SUCCESS" : {
            return {
                ...state,
                TopRatedMovie: action.result
            }
        }

        case "GET_NOW_PLAYING_MOVIE_SUCCESS" : {
            return {
                ...state,
                NowPlayingMovie: action.result,
                
            }
        }

        case "GET_POPULAR_MOVIE_SUCCESS" : {
            return {
                ...state,
                PopularMovie: action.result
            }
        }

        case "GET_POPULAR_TV_SUCCESS" : {
            return {
                ...state,
                PopularTVShows: action.result
            }
        }

        case "GET_UPCOMNING_MOVIE_SUCCESS" : {
            return {
                ...state,
                UpcomingMovie: action.result
            }
        }


        case "GET_NOW_PLAYING_MOVIE_FAIL" : {
            return {
                ...state,
                isFetching: false,
                TopRatedMovie: [],
                UpcomingMovie: [],
                NowPlayingMovie: [],
                PopularMovie: [],
                PopularTVShows: []
            }
        }

        default:
            return state 
    }
    
}