import { 
    fetchTopRatedMovie, 
    fetchUpcomingMovie, 
    fetchNowPlayingMovie,
    fetchPopularMovie, 
    fetchPopularTVShows 
} from '../../services/Api'

export function getTopRatedMovie() {
    return async dispatch=> {
        dispatch(requestState("GET_TOP_RATED_MOVIE"))
        try {
            const res = await fetchTopRatedMovie();
            console.log(res);
            if (res) {
                return dispatch(successState("GET_TOP_RATED_MOVIE_SUCCESS", res.results));
            }
            else {
                return dispatch(failedState("GET_TOP_RATED_MOVIE_FAIL"));
            }
        }
        catch (err) {
            console.log(err);
            return dispatch(failedState("GET_TOP_RATED_MOVIE_FAIL"));
        }
    }
}

export function getUpcomingMovie() {
    return async dispatch=> {
        dispatch(requestState("GET_UPCOMNING_MOVIE"))
        try {
            const res = await fetchUpcomingMovie();
            console.log(res);
            if (res) {
                return dispatch(successState("GET_UPCOMNING_MOVIE_SUCCESS", res.results));
            }
            else {
                return dispatch(failedState("GET_UPCOMNING_MOVIE_FAIL"));
            }
        }
        catch (err) {
            console.lo
            g(err);
            return dispatch(failedState("GET_UPCOMNING_MOVIE_FAIL_FAIL"));
        }
    }
}

export function getNowPlayingMovie() {
    return async dispatch=> {
        dispatch(requestState("GET_NOW_PLAYING_MOVIE"))
        try {
            const res = await fetchNowPlayingMovie();
            console.log(res);
            if (res) {
                return dispatch(successState("GET_NOW_PLAYING_MOVIE_SUCCESS", res.results));
            }
            else {
                return dispatch(failedState("GET_NOW_PLAYING_MOVIE_FAIL"));
            }
        }
        catch (err) {
            console.log(err);
            return dispatch(failedState("GET_NOW_PLAYING_MOVIE_FAIL"));
        }
    }
}


export function getPopularMovie() {
    return async dispatch=> {
        dispatch(requestState("GET_POPULAR_MOVIE"))
        try {
            const res = await fetchPopularMovie();
            console.log(res);
            if (res) {
                return dispatch(successState("GET_POPULAR_MOVIE_SUCCESS", res.results));
            }
            else {
                return dispatch(failedState("GET_POPULAR_MOVIE_FAIL"));
            }
        }
        catch (err) {
            console.log(err);
            return dispatch(failedState("GET_POPULAR_MOVIE_FAIL"));
        }
    }
}


export function getPopularTV() {
    return async dispatch=> {
        dispatch(requestState("GET_POPULAR_TV"))
        try {
            const res = await fetchPopularTVShows();
            console.log(res);
            if (res) {
                return dispatch(successState("GET_POPULAR_TV_SUCCESS", res.results));
            }
            else {
                return dispatch(failedState("GET_POPULAR_TV_FAIL"));
            }
        }
        catch (err) {
            console.log(err);
            return dispatch(failedState("GET_POPULAR_TV_FAIL"));
        }
    }
}





export function requestState(state) {
    return {
        type: state
    }
}

export function successState(type, result) {
    return {
        type: type,
        result: result
    }
}

export function failedState(type) {
    return {
        type: type,
        message: "Failed to fetch data!"
    }
}
