import React from 'react'
import { View, Text } from 'react-native'

import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import { persistor, store } from './src/redux/store'
import Route from './src/base/configs/routes'

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
          <Route/>
      </PersistGate>
    </Provider>
  )
}

export default App